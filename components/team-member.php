<?php 
    $even = ($index % 2) == 0;
?>

<div class="pure-g">
    <div class="team-member-info pure-u-1 pure-u-lg-1-2" <?php if($even) echo 'style="order:2"' ?>>
        <div class="team-member-header">
            <h1 style="color: <?php the_field('colour', $team_member->ID)?>"><?php the_field('name', $team_member->ID)?></h1>
            <h2 style="color: <?php the_field('colour', $team_member->ID)?>"><?php the_field('job_title', $team_member->ID)?></h2>
            <div style="background-color: <?php the_field('colour', $team_member->ID)?>" class="team-member-divider"></div>
        </div>
        <p><?php the_field('description', $team_member->ID)?></p>
        <div class="skills-section">
            <h1 style="color: <?php the_field('colour', $team_member->ID)?>">Specialises in</h1>
            <div class="icons">
                <?php $skills = get_field('skills', $team_member->ID);
                    foreach($skills as $post): 
                    setup_postdata($post);?>
                        <div class="skill-container">
                            <div class="icon skill">
                                <img class="style-svg centered" src="<?php echo get_field('icon')['url']; ?>"/>
                            </div>
                            <p><?php the_title(); ?></p>
                        </div>
                <?php endforeach; 
                wp_reset_postdata(); ?>
            </div>
        </div>
    </div>
    <div class="team-member-image pure-u-1 pure-u-lg-1-2" style="background-image: url(<?php echo get_field('photo', $team_member->ID)['url'] ?>)">

    </div>
</div>