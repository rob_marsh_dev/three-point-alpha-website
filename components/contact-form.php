<div class="contact-section">
    <div class="container pure-g">
        <div class="contact-description pure-u-lg-1-2">
            <p>Great! Reach out to us by filling our contact form with your details as much information
            on your project as possible and we’ll be in touch shortly after. We offer a free consultation
            to establish your requirements and propose ideas of how we can create your solution, specifically adapted to your budget.</p>
            <div class="contact-method vert-align">
                <img class="contact-icon" src="<?php bloginfo( 'template_url' )?>/assets/icons/e-mail-envelope.svg" />
                <p>hello@threepointalpha.co.uk</p>
            </div>
            <div class="contact-method vert-align">
                <img class="contact-icon" src="<?php bloginfo( 'template_url' )?>/assets/icons/phone-call.svg" />
                <p>07791 916437</p>
            </div>
        </div>
        <div id="contact-form" class="pure-u-lg-1-2">
            <?php echo do_shortcode("[wpforms id=86]");?>
        </div>
    </div>
</div>