<?php
    $class = '';
    if(is_page('our-team')):
        $class="team-banner";
    elseif(is_page('websites')):
        $class="primary";
    else:
        $class="secondary";
    endif;
    

    if ( have_posts() ) : while ( have_posts() ) : the_post();
        $pageDescription = get_the_content();
        $pageTitle = get_the_title();
        endwhile;
    endif;
        
?>

<div class="<?php echo $class ?> solid-banner">
    <div class="centered">
        <h5><?php echo $pageTitle.'.'?></h5>
        <h6><?php echo $pageDescription?></h6>
    </div>
</div>