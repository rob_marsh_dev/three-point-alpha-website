<?php /* Template Name: Web App Service Page */ ?>
<?php get_header(); ?>

<?php include 'components/service-banner.php' ?>

<div class="single-service__section">
    <div class="container">
        <div class="single-service__content">
            <h2 class="single-service__header">Types of Web Apps</h2>
            <div class="single-service__underline web-apps"></div>
            <div class="single-service__list types-of-web-apps">
                <div class="single-service__list-item pure-g vert-align">
                    <div class="pure-u-md-1-5 pure-u-sm-1-5 single-service__list-item-image">
                        <img src="<?php echo bloginfo( 'template_url' ); ?>/assets/icons/web-apps/cloud-hosted.svg">
                    </div>
                    <div class="pure-u-md-4-5 pure-u-sm-4-5">
                        <p><strong>Cloud hosted</strong> - Harness the power of the cloud by launching your application on servers specifically designed for rapid scalability, flexibility and availability. Thanks to 24/7 availability, maintenance and software upgrades are made easy. If adaptability and performance are important for your application, this is the platform for you.</p>
                    </div>
                </div>
                <div class="single-service__list-item pure-g vert-align">
                    <div class="pure-u-md-1-5 pure-u-sm-1-5 single-service__list-item-image">
                        <img src="<?php echo bloginfo( 'template_url' ); ?>/assets/icons/web-apps/internally-hosted.svg">
                    </div>
                    <div class="pure-u-md-4-5 pure-u-sm-4-5">
                        <p><strong>Internally hosted</strong> - Got your own server infrastructure in place? Perfect! Retain complete control of your data by installing your application internally. This way, you can manage your own data security by ensuring your application is behind your trusty firewall. If scalability and uptime aren’t absolutely essential, you could potentially save money by hosting internally.</p>
                    </div>
                </div>
                <div class="single-service__list-item pure-g vert-align">
                    <div class="pure-u-md-1-5 pure-u-sm-1-5 single-service__list-item-image">
                        <img src="<?php echo bloginfo( 'template_url' ); ?>/assets/icons/web-apps/web-api.svg">
                    </div>
                    <div class="pure-u-md-4-5 pure-u-sm-4-5">
                        <p><strong>Web API</strong> - Need a way to integrate or communicate data between a number of your custom systems? We can develop a solution to securely expose your data to authorised applications, creating a safe method of presenting data in a way that is ready for other applications to consume.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="single-service__content">
            <h2 class="single-service__header">Benefits of Web Apps</h2>
            <div class="single-service__underline web-apps"></div>
            <div class="single-service__list web-app-benefits pure-g">
                <div class="pure-u-md-1-2 pure-u-sm-1-2 single-service__list-item vert-align">
                    <div class="pure-u-sm-1-6 single-service__list-item-image">
                        <img src="<?php echo bloginfo( 'template_url' ); ?>/assets/icons/tick-green.svg">
                    </div>
                    <div class="pure-u-sm-5-6">
                        <p class="single-service__list-item-text">Automate mundane tasks and workflows.</p>
                    </div>
                </div>
                <div class="pure-u-md-1-2 pure-u-sm-1-2 single-service__list-item vert-align">
                    <div class="pure-u-sm-1-6 single-service__list-item-image">
                        <img src="<?php echo bloginfo( 'template_url' ); ?>/assets/icons/tick-green.svg">
                    </div>
                    <div class="pure-u-sm-5-6">
                        <p class="single-service__list-item-text">Reduced rate of human error.</p>
                    </div>
                </div>
                <div class="pure-u-md-1-2 pure-u-sm-1-2 single-service__list-item vert-align">
                    <div class="pure-u-sm-1-6 single-service__list-item-image">
                        <img src="<?php echo bloginfo( 'template_url' ); ?>/assets/icons/tick-green.svg">
                    </div>
                    <div class="pure-u-sm-5-6">
                        <p class="single-service__list-item-text">Free up resources to work on what you’re amazing at.</p>
                    </div>
                </div>
                <div class="pure-u-md-1-2 pure-u-sm-1-2 single-service__list-item vert-align">
                    <div class="pure-u-sm-1-6 single-service__list-item-image">
                        <img src="<?php echo bloginfo( 'template_url' ); ?>/assets/icons/tick-green.svg">
                    </div>
                    <div class="pure-u-sm-5-6">
                         <p class="single-service__list-item-text">Promotes collaborative and remote working for teams.</p>
                    </div>
                </div>
                 <div class="pure-u-md-1-2 pure-u-sm-1-2 single-service__list-item vert-align">
                    <div class="pure-u-sm-1-6 single-service__list-item-image">
                        <img src="<?php echo bloginfo( 'template_url' ); ?>/assets/icons/tick-green.svg">
                    </div>
                    <div class="pure-u-sm-5-6">
                        <p class="single-service__list-item-text">Administration panel for reporting and data analytics.</p>
                    </div>
                </div>
                <div class="pure-u-md-1-2 pure-u-sm-1-2 single-service__list-item vert-align">
                    <div class="pure-u-sm-1-6 single-service__list-item-image">
                        <img src="<?php echo bloginfo( 'template_url' ); ?>/assets/icons/tick-green.svg">
                    </div>
                    <div class="pure-u-sm-5-6">
                        <p class="single-service__list-item-text">Designed for browser, not for operating system (Update once, not 100 times).</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="single-service__block web-apps">
    <div class="pure-g">
        <div class="pure-u-lg-2-3 single-service__block-description">
            <h2>A Bespoke Service.</h2>
            <p>We understand that each business has unique challenges,
            so our software is designed and developed specifically to your needs.
            We don’t believe in shoe-horning our customers into using an existing product that doesn’t
            completely satisfy their needs. Because of this, you can trust that our web applications
            are customised to your requirements, guaranteeing you receive a bespoke application that behaves exactly how you envision it.</p>
        </div>
        <div class="pure-u-lg-1-3 centered-background-image" style="background-image: url(<?php echo bloginfo( 'template_url' ) ?>/assets/img/bespoke-service.jpg"></div>
    </div>
</div>
<div class="single-service__section align-center">
    <div class="container">
        <div class="single-service__content">
            <h2 class="single-service__header align-center">How we work</h2>
            <div class="single-service__underline web-apps"></div>
            <p>At Three Point Alpha, we believe in change. We offer Agile development services,
            ensuring the software we design will be adapted to your needs. </p> 
            <p>Our software development process is treated as an iterative process with regular 
            demonstrations, giving you the opportunity to test your custom solution before release and provide feedback.</p>
            <p>If you realise that a piece of functionality isn’t quite right, or that you’d like extra 
            features to be included, no problem! We will adapt our workflow to include the features that are most important for you.</p>
         </div>
    </div>
</div>
<div class="single-service__block support">
    <div class="pure-g">
        <div class="pure-u-sm-3-3 single-service__block-description">
           <div class="test">
            <h2>Support.</h2>
            <p>We believe that there is more to software development than creating your perfect solution, 
            handing it over and closing the door. Software needs maintenance and support, so we believe in 
            keeping a close relationship with you after the deployment of your software solution.</p>
            <p>You may discover small tweaks or changes to the user interface design that you wish to 
            make only after using your new system for a period of time. Because of this, we offer an extended 
            period in which we will make these changes, free of charge.</p>
            <p>Our software support services range far and wide, <a href="<?php echo get_page_link(106); ?>">take a look</a> at our support service page for more of an insight to what we offer.</p>
         </div>
         <img class="support-cog" src="<?php bloginfo('template_url') ?>/assets/icons/cog.svg"/>
        </div>

    </div>
</div>
<div class="single-service__section">
    <div class="container align-center">
        <h1>Interested?</h1>
        <a href="<?php echo get_page_link(83); ?>" class="btn service-web-app lowercase">Get in touch</a>
    </div>
</div>

<?php get_footer(); ?>