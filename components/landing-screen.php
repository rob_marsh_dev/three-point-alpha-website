<div class="centered-background-image landing-screen absolute-container">
    <img class="landing-screen__logo" src="<?php bloginfo('template_url'); ?>/assets/img/tpa-logo.png"/>
    <div class="container landing-screen__content">
        <h1>Custom Software Development.</h1>
        <h1>Cross-Platform Solutions.</h1>
        <h1>Personable Service.</h1>
    </div>
    <a data-scroll href="#testimonials-section" id="arrow-down"><img src="<?php echo bloginfo('template_url') ?>/assets/icons/arrow-down.svg"/></a>
</div>