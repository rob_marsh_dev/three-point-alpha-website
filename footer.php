			<footer>
				<div class="centered">
					<div class="icons">
						<a href="https://www.facebook.com/threepointalpha/">
							<div class="icon facebook">
								<img class="centered" src="<?php bloginfo('template_url'); ?>/assets/icons/facebook-logo.svg"/>
							</div>
						</a>
						<a href="https://twitter.com/ThreePointAlpha">
							<div class="icon twitter">
								<img class="centered" src="<?php bloginfo('template_url'); ?>/assets/icons/twitter-logo.svg"/>
							</div>
						</a>
					</div>
					<p id="footer-address">C4DI, 31-38 Queen St, Hull, HU1 1UU</p>
					<div class="footer-links">
						<?php wp_nav_menu( array(
							'menu' => 'footer-menu',
							'menu_class' => 'footer-navigation'
						)); ?>
					</div>
				</div>
				<span class="copyright">Copyright Three Point Alpha Ltd.</span>
			</footer>
			<?php wp_footer(); ?>
			<script>smoothScroll.init();</script>
		</body>
	</div>
</html>