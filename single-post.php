<?php 
    $authorID = get_post_field( 'post_author', $post->ID);
    $author = get_posts(array(
                    'post_type' => 'team_member',
                    'meta_query' => array(
                        array(
                            'key' => 'wp_username', // name of custom field
                            //TODO - Get author through $post and pass it into method
                            'value' => get_the_author_meta('user_login', $authorID)
                        )
                    )
                ));
    $authorName = $author[0]->post_title;
    if($author){
        $authorColour = get_field('colour', $author[0]->ID);
    }
?>

<?php get_header(); ?>
<?php include 'components/image-banner.php' ?>
<div class="single-blog-post sm-container">
    <div class="single-blog-meta pure-g vert-align">
        <div class="pure-u-md-3-4">
            <h5 class="single-blog-header" <?php if($authorColour): echo "style=color:".$authorColour.";"; endif;?>><?php the_title() ?></h5>
            <h6 class="single-blog-author">By <?php echo $authorName ?></h6>
        </div>
        <div class="pure-u-md-1-4">
            <?php echo get_avatar( get_the_author_meta( 'user_email'), 60, null, null, array('class'=>array('single-post__blog-author-img')));?>
        </div>
    </div>
    <div class="single-blog-content">
        <?php the_content() ?>
    </div>
</div>
<?php get_footer() ?>