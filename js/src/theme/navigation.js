window.onload = function(){
    var menuToggle = document.querySelector("#sm-menu-toggle"),
        pageContainer = document.querySelector("#page-container"),
        nav = document.querySelector(".sm-navigation"),
        closeNav = document.querySelector("#nav-close-icon");

    if(menuToggle !== null){
        menuToggle.addEventListener("click", function(){toggleMobileMenu();});
    }
    if(closeNav !== null) {
        closeNav.addEventListener("click", function(){toggleMobileMenu();});
    }

    function toggleMobileMenu() {
        if(nav !== null) {
            nav.classList.toggle("open");
        }
        if(pageContainer !== null) {
            pageContainer.classList.toggle("nav-open");
        }
    }
};