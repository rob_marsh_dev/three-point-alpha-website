# Three Point Alpha Website

This repository stores the WordPress theme for the Three Point Alpha website.

## Prerequisites

In order to view the theme on your local machine you will need the following:

* [Git](http://git-scm.com/)
* [Node.js](http://nodejs.org/) (with NPM)
* [Gulp](http://gulpjs.com/)
* [WordPress](https://wordpress.org/download/)

In addition to these you will also need a way of hosting the WordPress site locally. Typically we use XAMPP and follow this guide (https://premium.wpmudev.org/blog/setting-up-xampp/?omtr=b&utm_expid=3606929-101._J2UGKNuQ6e7Of8gblmOTA.1&utm_referrer=https%3A%2F%2Fwww.google.co.uk%2F) but you can use whichever method you prefer.

## Installation

* `git clone <repository-url>` this repository in the themes folder of your WP installation.
* `npm install` to install project dependencies.
* Add the following plugins from the company OneDrive:
    1. Advanced Custom Fields
    2. Custom Post Type UI
    3. SVG Support
    4. WPForms Lite
    5. WP Mail SMTP
* Take a backup of the live database and restore it to your local MySQL instance (typically [localhost/phpmyadmin]).(http://localhost/phpmyadmin)

## Development

* Run `gulp` in the root of the project whenever working on it.

## Publishing

* Run `gulp publish` to generate a production version of the site. It is generated in a folder called 'published' in the root.

## Deploying

The site is hosted on an AWS instance and should be accessed via an FTP client such as [FileZilla](https://filezilla-project.org/). The username is ubuntu and the private key can be found in the PEM Keys file of the company OneDrive.

## Contribution guidelines ##

* Ensure that you are only editing files in the src folders for images, css and js. This is necessary as gulp is watching these folders and will handle minification, compression and pre-processing. 
* If any errors are provided by jshint make sure that you fix them.

## Notes ##

* For a better understanding of the gulp workflow have a look at this tutorial that was followed in order to implement it https://code.tutsplus.com/tutorials/using-gulp-for-wordpress-automation--cms-23081