<?php /* Template Name: Support Service Page */ ?>
<?php get_header(); ?>

<?php include 'components/service-banner.php' ?>

<div class="single-service__section">
    <div class="container">
        <div class="single-service__content">
            <h2 class="single-service__header">Reap the benefits with our support</h2>
            <div class="single-service__underline support"></div>
            <p>Outsourcing your software support to us enables you to free up resources and spend more time doing what you love.</p>
            <div class="pure-g">
                <div class="support-service__benefit pure-u-sm-1-4 align-center">
                    <img src="<?php echo bloginfo( 'template_url' ) ?>/assets/icons/001-clock.svg"/>
                    <p>Outsourcing frees up your valuable time</p>
                </div>
                <div class="support-service__benefit pure-u-sm-1-4 align-center">
                    <img src="<?php echo bloginfo( 'template_url' ) ?>/assets/icons/002-graph.svg"/>
                    <p>Scalable software support packages.</p>
                </div>
                <div class="support-service__benefit pure-u-sm-1-4 align-center">
                    <img src="<?php echo bloginfo( 'template_url' ) ?>/assets/icons/003-handshake.svg"/>
                    <p>Only pay for what you use.</p>
                </div>
                <div class="support-service__benefit pure-u-sm-1-4 align-center">
                    <img src="<?php echo bloginfo( 'template_url' ) ?>/assets/icons/004-protection.svg"/>
                    <p>Emergency ad-hoc support available.</p>
                </div>
            </div>
        </div>
        <div class="single-service__content">
            <h2 class="single-service__header">Our support services</h2>
            <div class="single-service__underline support"></div>
            <p>We offer a wide range of support services, so you can pick and choose the best options that suit your organisation’s infrastructure. These include...</p>
            <h4>Software Support SLA Agreements</h4>
            <h4>Custom Software Maintenance</h4>
            <h4>Custom Software Upgrades and Improvements</h4>
            <h4>Ad-hoc Software Support</h4>
            <p>Whether you want a detailed SLA agreement or you have a new feature you’d like implementing into 
            one of your existing systems, we will give your custom application the care it needs to give your business 
            a boost. We even offer software support as an ad-hoc service, allowing you to buy our 
            time in 10 hour chunks as and when you need!</p>
        </div>
        <div class="container align-center">
            <h1>We can help.</h1>
            <a href="<?php echo get_page_link(83); ?>" class="btn service-support lowercase">Get in touch</a>
        </div>
    </div>
</div>

<?php get_footer(); ?>