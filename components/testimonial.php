<?php 
    if($index){
        //Need to check whether index exists it is only used when changing alignment of testimonials i.e. not in the carousel.
        $even = ($index % 2) == 0;
    }
?>

<div class="pure-g testimonial">
    <div <?php if($even) echo "style='order:2' "?> class="pure-u-md-3-4 testimonial-text-container">
        <?php the_content(); ?>
    </div>
    <div class="testimonial-img-container pure-u-md-1-4">
        <img class="testimonial-img centered" src="<?php echo get_field('company_logo', $testimonial->ID)['url']; ?>"/>
    </div>
</div>