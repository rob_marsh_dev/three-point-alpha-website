<?php get_header() ?>
<div class="content-404 centered">
    <img src="<?php bloginfo( 'template_url' ) ?>/assets/img/tpa-logo.png"/>
    <h1>Oops...</h1>
    <h4>The page you're looking for either doesn't exist, or has been removed.</h4>
    <p>Return to our <a href="<?php echo get_home_url(); ?>">Home Page</a> or if you think there is a problem, <a href="<?php echo get_page_link(83); ?>">Contact Us</a>.
</div>
<?php get_footer() ?>