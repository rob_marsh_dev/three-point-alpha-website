var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var cleanCss = require('gulp-clean-css');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');

var plumberErrorHandler = { errorHandler: notify.onError({
  title: 'Gulp',
  message: 'Error <%= error.message %>'
})};

gulp.task('sass', function() {
  gulp.src('./css/src/main.scss')
    .pipe(plumber(plumberErrorHandler))
    .pipe(sass())
    .pipe(concat('main.css'))
    .pipe(cleanCss())
    .pipe(gulp.dest('./css'));
});

gulp.task('js', function() {
  gulp.src(['js/src/vendor/*.js', 'js/src/theme/*.js'], {base: 'js/'})
    .pipe(plumber(plumberErrorHandler))
    .pipe(jshint())
    .pipe(jshint.reporter('jshint-stylish'))
    .pipe(jshint.reporter('fail'))
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./js'));
});

gulp.task('img', function() {
  gulp.src(['./assets/img/src/*.{png,jpg,jpeg,gif}'])
    .pipe(plumber(plumberErrorHandler))
    .pipe(imagemin({
      progressive: true
    }))
    .pipe(gulp.dest('./assets/img'));
}); 

gulp.task('watch', function() {
  gulp.watch('css/src/**/*.scss', ['sass']);
  gulp.watch('js/src/theme/*.js', ['js']);
  gulp.watch('js/src/vendor/*.js', ['js']);
  gulp.watch('assets/img/src/*.{png,jpg,JPG,jpeg,gif}', ['img']);
});

gulp.task('publish', function() {
  gulp.src(['./*.{php,css}'])
    .pipe(gulp.dest('./published'));
  gulp.src(['./assets/fonts/*'])
    .pipe(gulp.dest('./published/assets/fonts'));
  gulp.src(['./assets/img/*.{png,jpg,JPG,jpeg,gif}'])
    .pipe(gulp.dest('./published/assets/img'));
  gulp.src(['./assets/icons/**/*'])
    .pipe(gulp.dest('./published/assets/icons'));
  gulp.src(['./components/*.php'])
    .pipe(gulp.dest('./published/components'));
  gulp.src(['./css/*.css'])
    .pipe(gulp.dest('./published/css'));
  gulp.src(['./js/*.js'])
    .pipe(gulp.dest('./published/js'));
})

gulp.task('default', ['sass', 'js', 'img', 'watch']);