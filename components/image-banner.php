<?php

//If it is the blog page you need to pull the page title and content differently, using the following method.
if(is_home()) {
    $image = get_field('image', get_post(get_option( 'page_for_posts' )) );
    $image_url = $image['url'];
    $header = get_field('header', get_option( 'page_for_posts' ));
    $subheader = get_field('subheader', get_option( 'page_for_posts' ));
    $overlay_colour = get_field('overlay_colour', get_option( 'page_for_posts' ));
}
else {
    if( have_posts() ) : while ( have_posts() ) : the_post();
        $image = get_field('image', $post);
        $image_url = $image['url'];        
        $header = get_field('header', $post);
        $subheader = get_field('subheader', $post);
        $overlay_colour = get_field('overlay_colour', $post);
    endwhile; endif;
}
?>

<div class="centered-background-image image-banner <?php echo $overlay_colour ?>" style="background-image: url(<?php echo $image_url ?>)">
    <div class="centered">
        <?php if($header != null): ?>
            <h5><?php echo $header ?></h5>
        <?php endif;?>
        <?php if($subheader != null): ?>
            <h6><?php echo $subheader ?></h6>
        <?php endif; ?>
    </div>
</div>