<?php /* Template Name: Contact Page */ ?>
<?php get_header();?>

<?php include 'components/image-banner.php' ?>
<?php include 'components/contact-form.php' ?>

<div class="contact-section">
    <div class="container pure-g">
        <div class="contact-description pure-u-lg-1-2">
            <h2>  
                Alternatively, come and see us.
            </h2>
            <p> 
                We are situated in the wonderful C4DI building, located in Hull’s Fruit Market. Why not arrange to pay us a visit and discuss your ideas over a coffee?
            </p>
        </div>
        <div id="contact-bottom-right" class="pure-u-lg-1-2 map-section">
            <iframe id="map" src="https://www.google.com/maps/embed/v1/place?q=three%20point%20alpha&key=AIzaSyBWnMNXydyS3kS1BDoNRJDX2rT3OizwfTs" allowfullscreen></iframe>
        </div>
    </div>
</div>
<?php get_footer() ?>