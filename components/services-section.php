<div class="service websites">
    <div class="pure-g">
        <div class="pure-u-lg-1-2 service-overview">
            <h1>Websites</h1>
            <h2>Professional | Content Management | E-commerce Platform.</h2>
            <p>Need a new e-commerce store to sell your goods? Or maybe you’re a blogger and want a new CMS platform to engage your readers. Perhaps you need a simple website designed to advertise your product and portray your brand. No matter what you need, we have you/it covered/we cover it all. Let our technical team help turn your vision into a reality.</p>
            <p>From £279</p>
            <a href="<?php echo get_page_link(68); ?>" class="btn primary">Our Packages</a>
        </div>
        <div class="browser-window">
            <div class="browser-bar">
                <div class="browser-button red"></div>
                <div class="browser-button amber"></div>
                <div class="browser-button green"></div>
            </div>
            <img src="<?php bloginfo('template_url'); ?>/assets/img/ross-oliver-website.jpg">
        </div>
    </div>
</div>

<div class="service web-apps">
    <div class="pure-g">
        <div class="pure-u-lg-1-2 service-overview">
            <h1>Web Apps</h1>
            <h2>Workflows | Cloud Based | Web API</h2>
            <p>Discovered a time-consuming task in your business that technology could make more efficient? Or maybe you’re relying on Excel or Microsoft Access to keep your data organized. Perhaps you need a solution to securely expose your data to a number of platforms or pieces of software? Our developers have years of development experience creating custom web applications and solutions. Let us help.</p>
            <p>From £949</p>
            <a href="<?php echo get_page_link(89); ?>" class="btn secondary">Learn More</a>
        </div>
        <div class="pure-u-lg-1-2 web-app-icons">
            <div class="pure-g">
                <div class="pure-u icon-container__1-3">
                    <img class="web-app-icon" src="<?php bloginfo('template_url'); ?>/assets/icons/user.svg">
                </div>
                <div class="pure-u icon-container__1-3">
                    <img class="web-app-icon" src="<?php bloginfo('template_url'); ?>/assets/icons/user.svg">
                </div>
                <div class="pure-u icon-container__1-3">
                    <img class="web-app-icon" src="<?php bloginfo('template_url'); ?>/assets/icons/user.svg">
                </div>
                <div class="pure-u icon-container__1-1 margin">
                    <img class="web-app-icon" src="<?php bloginfo('template_url'); ?>/assets/icons/api.svg">
                </div>
                <div class="pure-u icon-container__1-2">
                    <img class="web-app-icon" src="<?php bloginfo('template_url'); ?>/assets/icons/server.svg">
                </div>
                <div class="pure-u icon-container__1-2">
                    <img class="web-app-icon" src="<?php bloginfo('template_url'); ?>/assets/icons/database.svg">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="service support">
    <div class="pure-g">
        <div class="pure-u-lg-1-2 service-overview">
            <h1>Support</h1>
            <h2>Maintenance | Third-Line | Ad-hoc Support</h2>
            <p>Do you have a custom software solution that needs to be maintained or developed upon? Want to outsource your database or third line support in order to free up more resources for your current projects? We have years of experience supporting legacy software for clients, let us show you why.</p>
            <p>From £30ph</p>
            <a href="<?php echo get_page_link(106); ?>" class="btn quaternary">We Can Help</a>
        </div>
        <img class="support-cog" src="<?php bloginfo('template_url'); ?>/assets/icons/cog.svg">
    </div>
</div>