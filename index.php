<?php get_header(); ?>
<?php include 'components/landing-screen.php' ?>

<!--Testimonials Block-->
<?php 
    $testimonials = get_field('testimonials', $post);
?>
<div id="testimonials-section" class="landing-page__testimonial-block">
    <div class="container">
        <div class="testimonials">
            <?php $index = 0;
            foreach($testimonials as $testimonial):
                setup_postdata($testimonial); 
                $index++;
                include 'components/testimonial.php';
            endforeach;
            wp_reset_postdata();
             ?>
        </div>
    </div>
</div>

<!--Our Services-->
<div class="landing-page__container container">
    <h1 class="landing-page__service-header">What we offer</h1>
    <div class="landing-page__services pure-g">
        <div class="landing-page__service pure-u-lg-1-4 pure-u-md-5-12">
            <div class="landing-page__service-image centered-background-image absolute-container" style="background-image:url(<?php bloginfo('template_url'); ?>/assets/img/web-app-specialist.jpeg)">
                <h2 class="centered">Websites.</h2>
            </div>
            <p>Whether you need a website for blogging, a content management system or a fully fledged e-commerce store developed, we’ve got it covered. Our team has the expertise to turn your vision into a reality.</p>
            <a href="<?php echo get_page_link(68); ?>" class="btn service-website lowercase">Learn More</a>
        </div>
        <div class="pure-u-lg-1-8 pure-u-md-1-6"></div>
        <div class="landing-page__service pure-u-lg-1-4 pure-u-md-5-12">
            <div class="landing-page__service-image centered-background-image absolute-container" style="background-image:url(<?php bloginfo('template_url'); ?>/assets/img/web-app-image.jpg)">
                <h2 class="centered">Web Apps.</h2>
            </div>
            <p>Our developers have years of development experience creating custom web applications and solutions. Let us help.</p>
            <a href="<?php echo get_page_link(89); ?>" class="btn service-web-app lowercase">Learn More</a>
        </div>
        <div class="pure-u-lg-1-8"></div> 
        <div class="landing-page__service pure-u-lg-1-4 pure-u-md-1">
            <div class="landing-page__service-image centered-background-image absolute-container" style="background-image:url(<?php bloginfo('template_url'); ?>/assets/img/support-image.jpeg)">
                <h2 class="centered">Support.</h2>
            </div>
            <p>Want to outsource your database or third line support in order to free up more resources for your current projects? We have years of experience supporting legacy software for clients, let us show you why.</p>
            <a href="<?php echo get_page_link(106); ?>" class="btn service-support lowercase">Learn More</a>
        </div>                      
        <!--<div class="landing-page__service align-center pure-u-md-1-3">
            <img src="<?php bloginfo( 'template_url' ) ?>/assets/img/tpa-logo.png"/>
            <h2>Websites</h2>
            <div class="landing-page__divider"></div>
            <div class="landing-page__service-benefits">
                <div class="landing-page__service-benefit pure-g vert-align">
                    <img src="<?php bloginfo( 'template_url' )?>/assets/icons/tick-green.svg"/>
                    <p>PERSONAL</p>
                </div>
                <div class="landing-page__service-benefit pure-g vert-align">
                    <img src="<?php bloginfo( 'template_url' )?>/assets/icons/tick-green.svg"/>
                    <p>CMS</p>
                </div>
                <div class="landing-page__service-benefit pure-g vert-align">
                    <img src="<?php bloginfo( 'template_url' )?>/assets/icons/tick-green.svg"/>
                    <p>E-COMMERCE</p>
                </div>
            </div>
        </div>
        <div class="landing-page__service align-center pure-u-md-1-3">
            <img src="<?php bloginfo( 'template_url' ) ?>/assets/img/tpa-logo.png"/>
            <h2>Web apps</h2>
            <div class="landing-page__divider"></div>
            <div class="landing-page__service-benefits">
                <div class="landing-page__service-benefit pure-g vert-align">
                    <img src="<?php bloginfo( 'template_url' )?>/assets/icons/tick-green.svg"/>
                    <p>CORPORATION</p>
                </div>
                <div class="landing-page__service-benefit pure-g vert-align">
                    <img src="<?php bloginfo( 'template_url' )?>/assets/icons/tick-green.svg"/>
                    <p>DATABASE</p>
                </div>
                <div class="landing-page__service-benefit pure-g vert-align">
                    <img src="<?php bloginfo( 'template_url' )?>/assets/icons/tick-green.svg"/>
                    <p>API</p>
                </div>
            </div>
        </div>
        <div class="landing-page__service align-center pure-u-md-1-3">
            <img src="<?php bloginfo( 'template_url' ) ?>/assets/img/tpa-logo.png"/>
            <h2>Support</h2>
            <div class="landing-page__divider"></div>
            <div class="landing-page__service-benefits">
                <div class="landing-page__service-benefit pure-g vert-align">
                    <img src="<?php bloginfo( 'template_url' )?>/assets/icons/tick-green.svg"/>
                    <p>LEGACY</p>
                </div>
                <div class="landing-page__service-benefit pure-g vert-align">
                    <img src="<?php bloginfo( 'template_url' )?>/assets/icons/tick-green.svg"/>
                    <p>TICKETS</p>
                </div>
                <div class="landing-page__service-benefit pure-g vert-align">
                    <img src="<?php bloginfo( 'template_url' )?>/assets/icons/tick-green.svg"/>
                    <p>PRODUCTION</p>
                </div>
            </div>
        </div>-->
    </div>
</div>

<!--Our Team-->
<div class="pure-g">
        <div class="landing-page__our-team-content pure-u-md-1-2">
        <h2>Web and cross platform development specialists across Hull and East Yorkshire.</h2>
        <p>We are a Hull based custom software development company located at the Centre for Digital Innovation (C4DI), the largest tech hub in the north of England.</p>
        <p>With specialties in web design, cross platform web development, and custom software development, we have all bases covered. We appreciate that every business has unique requirements and challenges, so we take the time to connect with you.</p>
        <p>We can guarantee that you will receive a custom system designed to your exact requirements, giving your business the boost it needs to reach full potential.</p>
        <a href="<?php echo get_page_link(7); ?>" class="btn service-support lowercase">Meet our team</a>
    </div>
    <div class="centered-background-image landing-page__our-team-image pure-u-md-1-2" style="background-image:url(<?php bloginfo('template_url'); ?>/assets/img/web-app-specialist.jpeg)">

    </div>
</div>

<!--Contact Form-->
<h1 class="landing-page__contact-header align-center">Interested in working with us?</h1>
<?php include 'components/contact-form.php' ?>

<?php get_footer(); ?>