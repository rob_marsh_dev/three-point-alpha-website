<?php /* Template Name: Team Page */ ?>
<?php get_header(); ?>
<?php include 'components/solid-banner.php' ?>
<?php 
    $team_members = get_field('team_members', $post);
    $testimonials = get_field('testimonials', $post);
?>
<?php $index = 0;
    foreach($team_members as $team_member):
    $index++;
    include 'components/team-member.php';
endforeach; ?>

<div class="testimonial-block">
    <div class="container">
        <h1>Don't take our word for it.</h1>
        <h2>See what these lovely people had to say about our team.</h2>
        <div class="testimonials">
            <?php $index = 0;
            foreach($testimonials as $testimonial):
                setup_postdata($testimonial); 
                $index++;
                include 'components/testimonial.php';
            endforeach;
            wp_reset_postdata();
             ?>
        </div>
    </div>
</div>
<?php get_footer(); ?>