<?php
	function tpa_theme_styles(){
		wp_enqueue_style('style_css', get_template_directory_uri().'/style.css');
		wp_enqueue_style('main_css', get_template_directory_uri().'/css/main.css');
	}

	function tpa_theme_scripts(){
		wp_enqueue_script('app_js', get_template_directory_uri().'/js/app.js', '', '', true);
	}

	function tpa_menu_registration(){
		register_nav_menus( 
			array( 
				'header-menu' => __( 'Header Menu' ),
				'mobile-menu' => __( 'Mobile Menu' ),
				'footer-menu' => __( 'Footer Menu' )
			)
		 );
	}

	add_theme_support( 'post-thumbnails' );

	remove_filter('acf_the_content', 'wpautop');
	remove_filter('the_excerpt', 'wpautop');

	add_action('wp_enqueue_scripts', 'tpa_theme_styles');
	add_action('wp_enqueue_scripts', 'tpa_theme_scripts');
	add_action('init', 'tpa_menu_registration');
?>