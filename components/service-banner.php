<?php 
    if( have_posts() ) : while ( have_posts() ) : the_post();
        $image = get_field('image', $post);
        $image_url = $image['url'];        
        $header = get_field('header', $post);
        $description = get_field('description', $post);
        $has_button = get_field('has_button', $post);
        if($has_button):
            $button_text = get_field('button_text', $post);
            $button_colour = get_field('button_colour', $post);
            $button_link = get_field('button_link', $post);
        endif;
        $has_sub_banner = get_field('has_sub_banner', $post);
        if($has_sub_banner):
            $sub_banner_colour = get_field('sub_banner_colour');
            $sub_banner_text = get_field('sub_banner_text');
        endif;
    endwhile; endif;
?>

<div class="centered-background-image service-banner" style="background-image: url(<?php echo $image_url ?>)">
    <div class="pure-g">
        <div class="service-banner__content raised-effect pure-u-md-1-2">
            <h2><?php echo $header ?></h2>
            <p><?php echo $description ?></p>
            <?php if($has_button): ?>
                <a href="<?php echo $button_link ?>" class="btn lowercase" style="background-color: <?php echo $button_colour ?>"><?php echo $button_text ?></a>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php if ($has_sub_banner): ?>
    <div class="sub-service-banner__container" style="background-color: <?php echo $sub_banner_colour ?>">
        <h2><?php echo $sub_banner_text ?></h2>
    </div>
<?php endif; ?>