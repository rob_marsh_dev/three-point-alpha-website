<?php
    $isFeaturedPost = in_category('featured');
    $image = get_field('image', get_post());
    $image_url = $image['url'];

    $featuredImage = 'style="background-image: url('.$image_url.')"';
    $author = get_posts(array(
                    'post_type' => 'team_member',
                    'meta_query' => array(
                        array(
                            'key' => 'wp_username', // name of custom field
                            'value' => get_the_author_meta('user_login')
                        )
                    )
                ));
    if($author){
        $authorColour = get_field('colour', $author[0]->ID);
    }
 ?>

<div class="blog-post <?php if($isFeaturedPost) echo "featured pure-g" ?>">
    <a href="<?php the_permalink(); ?>"></a>
    <div <?php echo $featuredImage ?> class="centered-background-image blog-image <?php if($isFeaturedPost) echo "pure-u-sm-5-8" ?>">

    </div>
    <div class="blog-info <?php if($isFeaturedPost) echo "pure-u-sm-3-8" ?>">
        <div class="flex-centered-container">
            <h6 class="blog-header" <?php if($authorColour): echo "style=color:".$authorColour.";"; endif;?>><?php the_title(); ?>.</h6>
            <span class="spacer"></span>
            <?php echo get_avatar( get_the_author_meta( 'user_email'), 40, null, null, array('class'=>array('md-blog-author-img')));?>
        </div>
        <p class="blog-excerpt"><?php the_excerpt(); ?></p>
        <div class="flex-centered-container blog-meta">
            <?php echo get_avatar( get_the_author_meta( 'user_email'), 75, null, null, array('class'=>array('blog-author-img')));?>
            <span class="spacer"></span>
            <a <?php if($authorColour): echo "style=background-color:".$authorColour.";"; endif;?> class="btn blog" href="<?php the_permalink(); ?>">Read more</a>
        </div>
    </div>
</div>