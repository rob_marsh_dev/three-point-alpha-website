<?php $isHomePage = is_front_page(); ?>

<!doctype html>
<html class="no-js" lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title><?php wp_title(' | ', true, 'right'); bloginfo(('name')) ?></title>
	<?php wp_head(); ?>
</head>

<div class="sm-navigation">
	<a id="nav-close-icon">X</a>
	<?php wp_nav_menu( array(
		'menu' => 'mobile-menu',
		'menu_class' => 'sm-menu'
	)); ?>
</div>

<div id="page-container">
	<header class="<?php if($isHomePage):?>landing-header <?php endif; ?>vert-align">
		<a href=<?php echo get_home_url() ?>>
			<img class="header-logo" src="<?php bloginfo('template_url'); ?>/assets/img/tpa-logo.png">
		</a>
		<span class="spacer"></span>
		<?php wp_nav_menu( array( 
			'menu' => 'header-menu',
			'menu_class' => 'md-navigation')); ?>
		<div id="sm-menu-toggle" class="<?php if($isHomePage):?>landing-header<?php endif; ?> vert-align">
			<span>Menu</span>
			<?php if($isHomePage):?>
				<img src="<?php bloginfo('template_url'); ?>/assets/icons/menu-icon-white.svg"/>
			<?php else: ?>
				<img src="<?php bloginfo('template_url'); ?>/assets/icons/menu-icon.svg"/>
			<?php endif; ?>
		</div>
	</header>
		<body <?php body_class() ?>>