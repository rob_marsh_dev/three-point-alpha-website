<?php /* Template Name: Website Service Page */ ?>
<?php get_header(); ?>

<div class="align-center">
    <h1 class="section-header">Our Packages</h1>
</div>

<div class="package pure-g">
    <div class="pure-u-lg-1-2">
        <div class="package-header vert-align">
            <img src="<?php bloginfo('template_url'); ?>/assets/icons/basic-package.svg">
            <h4>Basic</h4>
        </div>
        <p>A simple website is perfect for boosting your professional credibility whilst increasing your number of potential leads. 
            With little maintenance or upkeep required, this package will serve as a cost-effective marketing solution ensuring your website and brand reach your customers. 
            Our responsive web design ensures your website will be optimised for cross-platform viewing, whether through the web browser, mobile or on tablet. </p>
         <div class="technology-icons">
            <img src="<?php bloginfo('template_url'); ?>/assets/icons/technologies/html-5.svg">
            <img src="<?php bloginfo('template_url'); ?>/assets/icons/technologies/css-3.svg">
         </div>
    </div>
    <div class="pure-u-lg-1-2 benefits">
        <div class="centered-benefits">
            <div class="benefit vert-align">
                <img src="<?php bloginfo('template_url'); ?>/assets/icons/tick-white.svg">
                <h6>Cost-effective, 24/7 marketing.</h6>
            </div>
            <div class="benefit vert-align">
                <img src="<?php bloginfo('template_url'); ?>/assets/icons/tick-white.svg">
                <h6>Low maintenance</h6>
            </div>
            <div class="benefit vert-align">
                <img src="<?php bloginfo('template_url'); ?>/assets/icons/tick-white.svg">
                <h6>Promte your brand and attract custom.</h6>
            </div>
            <div class="benefit vert-align">
                <img src="<?php bloginfo('template_url'); ?>/assets/icons/tick-white.svg">
                <h6>Boost your online presence and professional credibility.</h6>
            </div>
        </div>
    </div>
    <div class="package-price align-center">
        <h5>From £249</h5>
        <a href="<?php echo get_page_link(83); ?>" class="btn primary">
            Get in Touch
        </a>
    </div>
</div>

<div class="package pure-g">
    <div class="pure-u-lg-1-2">
        <div class="package-header vert-align">
            <img src="<?php bloginfo('template_url'); ?>/assets/icons/cms-package.svg">
            <h4>CMS</h4>
        </div>
        <p>This package is great for pushing your own customised content for your viewers. Creating content through blogs can really benefit your SEO, moving your website ahead of your competition in the google search rankings.</p>
        <p>Want to change your advertised contact number? Got a new product that you want to show off to your customers? No problem! Our solution will make it easy for you to make any tweaks to your website yourself, without the need for any technical expertise.</p>
         <div class="technology-icons">
            <img src="<?php bloginfo('template_url'); ?>/assets/icons/technologies/wordpress.svg">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/umbraco.png">
            <img src="<?php bloginfo('template_url'); ?>/assets/img/joomla.png">
         </div>
    </div>
    <div class="pure-u-lg-1-2 benefits">
        <div class="centered-benefits">
            <div class="benefit vert-align">
                <img src="<?php bloginfo('template_url'); ?>/assets/icons/tick-green.svg">
                <h6>Boost your website's SEO ranking.</h6>
            </div>
            <div class="benefit vert-align">
                <img src="<?php bloginfo('template_url'); ?>/assets/icons/tick-green.svg">
                <h6>Create your own content.</h6>
            </div>
            <div class="benefit vert-align">
                <img src="<?php bloginfo('template_url'); ?>/assets/icons/tick-green.svg">
                <h6>Delegate site responsibilities across your team.</h6>
            </div>
            <div class="benefit vert-align">
                <img src="<?php bloginfo('template_url'); ?>/assets/icons/tick-green.svg">
                <h6>Reduced maintenance costs with non-technical, efficient updates to your website.</h6>
            </div>
        </div>
    </div>
    <div class="package-price align-center">
        <h5>From £749</h5>
        <a href="<?php echo get_page_link(83); ?>" class="btn primary">
            Get in Touch
        </a>
    </div>
</div>

<div class="package pure-g">
    <div class="pure-u-lg-1-2">
        <div class="package-header vert-align">
            <img src="<?php bloginfo('template_url'); ?>/assets/icons/e-commerce-package.svg">
            <h4>E-Commerce</h4>
        </div>
        <p>Our E-commerce package allows your shop to remain open for custom 24/7, greatly increasing your opportunity to make sales. E-commerce stores remove the financial overhead of a physical store, making it a cost effective way to expand your business.</p>
        <p>With online sales increasing, it’s more important than ever to have a strong online presence in order increase your brand reputability. Our E-Commerce store development package will give you a professional sales platform that will complement a strong social media marketing campaign perfectly.</p>
         <div class="technology-icons">
            <img id="shopify-logo" src="<?php bloginfo('template_url'); ?>/assets/icons/technologies/shopify.svg">
         </div>
    </div>
    <div class="pure-u-lg-1-2 benefits">
        <div class="centered-benefits">
            <div class="benefit vert-align">
                <img src="<?php bloginfo('template_url'); ?>/assets/icons/tick-white.svg">
                <h6>Perfect for small businesses.</h6>
            </div>
            <div class="benefit vert-align">
                <img src="<?php bloginfo('template_url'); ?>/assets/icons/tick-white.svg">
                <h6>Complements social media marketing.</h6>
            </div>
            <div class="benefit vert-align">
                <img src="<?php bloginfo('template_url'); ?>/assets/icons/tick-white.svg">
                <h6>Increase profits with 24/7 access.</h6>
            </div>
            <div class="benefit vert-align">
                <img src="<?php bloginfo('template_url'); ?>/assets/icons/tick-white.svg">
                <h6>Increase your geographic reach.</h6>
            </div>
        </div>
    </div>
    <div class="package-price align-center">
        <h5>From £1349</h5>
        <a href="<?php echo get_page_link(83); ?>" class="btn primary">
            Get in Touch
        </a>
    </div>
</div>

<div class="process-section">
    <div class="overlap"></div>
    <div class="process align-center">
        <h1 class="section-header">The Process</h1>
        <div class="process-step">
            <div class="process-header flex-center">
                <img src="<?php bloginfo('template_url'); ?>/assets/icons/design.svg">
                <h4>Design.</h4>
            </div>
            <p>We understand the importance of communication. We take the time to talk to you and identify your needs as an individual, as well as your brand and message.</p>
            <p>Our specialist designers use cutting edge tools to create you a modern, custom website design that accurately represents your brand.</p>
        </div>
        <img class="arrow" src="<?php bloginfo('template_url'); ?>/assets/icons/arrow.svg">
        <div class="process-step">
            <div class="process-header flex-center">
                <img src="<?php bloginfo('template_url'); ?>/assets/icons/develop.svg">
                <h4>Develop.</h4>
            </div>
            <p>Our professional web developers start work using the latest technologies in order to turn these designs into a high quality, professional solution.</p>
            <p>We endorse the use of Agile methodologies with our software development, so we offer you regular demonstrations so you can suggest changes mid-development.</p>
        </div>
        <img class="arrow" src="<?php bloginfo('template_url'); ?>/assets/icons/arrow.svg">
        <div class="process-step">
            <div class="process-header flex-center">
                <img src="<?php bloginfo('template_url'); ?>/assets/icons/deploy.svg">
                <h4>Deploy.</h4>
            </div>
            <p>Need your website hosting to be set up for your? No problem! We’ll get you set up with a suitable hosting provider for your needs.</p>
            <p>Maybe you want to handle hosting yourself, in which case we’ll hand over your solution ready for you to deploy.</p>
            <p>Either way, your website is ready to go live!</p>
        </div>
        <img class="arrow" src="<?php bloginfo('template_url'); ?>/assets/icons/arrow.svg">
        <img class="happy-client" src="<?php bloginfo('template_url'); ?>/assets/icons/happy-client.svg">
    </div>
</div>

<?php get_footer(); ?>