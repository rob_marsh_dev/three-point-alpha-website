<?php get_header(); ?>
<?php include 'components/image-banner.php' ?>

<div class="container">

    <div class="align-center">
        <h1 class="section-header">Featured</h1>
    </div>

    <?php $featuredPosts = get_posts(array('category_name'=>'featured')); 
        $featuredPostsIDs = array();
        foreach($featuredPosts as $post){
            array_push($featuredPostsIDs, $post->ID);
            setup_postdata( $post );
            include 'components/blog.php';
        }
        wp_reset_postdata();
    ?>

    <div class="align-center">
        <h1 class="section-header">Other Blogs</h1>
    </div>

    <?php 
        $otherPosts = new WP_Query( array( 
            'post_type' => 'post',
            'post__not_in' => $featuredPostsIDs ));
        if(sizeof($otherPosts->posts) == 0): ?>
            <h2 style="text-align: center;">More blogs coming soon!</h2>
        <?php
            else: ?>
            <div class="blog-collection pure-g"><?php 
                foreach($otherPosts->posts as $post):
                    setup_postdata($post); ?>
                    <div class="pure-u-lg-1-3 pure-u-sm-1-2"><?php 
                        include 'components/blog.php'; ?>
                    </div><?php
                endforeach;
            endif; ?>
            </div>
        <?php wp_reset_postdata();
    ?>
</div>

<?php get_footer(); ?>