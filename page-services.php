<?php /* Template Name: Services Page */ ?>
<?php get_header(); ?>
<?php include 'components/services-section.php' ?>
<div class="pure-g final-services-section">
    <div class="pure-u-md-5-8">
        <h6>After something else?</h6>
        <p>These services are just the tip of the iceberg! We have an extensive knowledge in a variety of technologies and languages that haven’t been listed. If you have a need that hasn’t been identified then please get in touch, we can still help you.</p>
    </div>
    <div class="pure-u-md-3-8 btn-container">
        <a href="<?php echo get_page_link(83); ?>" class="btn quaternary">Get in Touch</a>
    </div>
</div>
<!-- TODO - Add fourth section -->
<?php get_footer() ?>